const GemModel = require('../../models/gems')
const Random = require('meteor-random')
const cloudinary = require('cloudinary').v2
cloudinary.config({ 
    cloud_name: 'ravenegg', 
    api_key: '173273979277351', 
    api_secret: 'zGjYH6vwUSalJPm2sgSevqUMNaM' 
  });


module.exports = {
    insertGem,
    getAllGems,
    getGem,
    updateGem,
    deleteGem,
    insertReview,
    uploadImagetoGem
}

async function insertGem(req, res){
    let id = Random.id()
        const {name, description, long_description, price, size, specs, reviews} = req.body
        const newGem = new GemModel({name, description, long_description, price, size, specs, reviews})
        newGem._id = id
        await newGem.save()
        res.status(200).send({_id: id, success_msg: 'New gem created successfull'})
}

async function getAllGems(req, res){
    let gems = await GemModel.find({})
    let total = await GemModel.find({}).countDocuments()
    res.status(200).send({gems: gems, total: total})
}

async function getGem(req, res){
    let gem = await GemModel.findOne({"_id": req.params._id})
    res.status(200).send({gem: gem})
}

async function updateGem(req, res){
    const {_id, name, description, long_description, price, size, canPurchase, favorite, specs} = req.body
    await GemModel.findByIdAndUpdate(_id, 
        {
            name, 
            description, 
            long_description, 
            price, 
            canPurchase, 
            favorite,
            size, 
            specs
        })
    res.status(200).send({'success_msg': 'Gem Updated Successfully'})
}

async function deleteGem(req, res){
    await GemModel.findByIdAndDelete(req.params._id)
    res.status(200).send({'success_msg':'Gem Removed Successfully'})
}

async function insertReview(req, res){
    let gemId = req.body._id
    let update = req.body.review
    GemModel.findByIdAndUpdate(gemId,
        {"$push": {"reviews": update}},
        {"new": false, "upsert": true},
        function (err, conceptUpdated) {
            if (err) res.status(500).send({message: `Error in the request ${err}`})
            res.status(200).send({'success_msg': `Review inserted successfully - ${conceptUpdated}`})
        }
    );
}

function uploadImagetoGem(req, res){
   let _id = req.body._id
   let file = req.files.file
   let key = new Date().toISOString()

   cloudinary.uploader.upload(file.path,
    { public_id: `gems/${key}`, tags: `gems` }, // directory and tags are optional
    function(err, image) {
      if (err) return res.send(err)
      console.log('file uploaded to Cloudinary')
      // remove file from server
      const fs = require('fs')
      fs.unlinkSync(file.path)
      updateImages(_id, image.url)
      // return image details
      res.json(image)
    }
  )
}

function updateImages(id, url, res) {
    let conceptId = id
    let update = url
    GemModel.findByIdAndUpdate(conceptId,
        {"$push": {"images": update}},
        {"new": false, "upsert": true},
        function (err, conceptUpdated) {
            if (err) res.status(500).send({message: `Error in the request ${err}`});
            console.log("gem", conceptUpdated);
        }
    );
}
