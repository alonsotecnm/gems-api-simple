const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const Random = require('meteor-random')
const morgan = require('morgan')

//Initializations
const app = express()
require('./database')

//Settings
app.set('port', process.env.PORT || 3000)
app.use(cors());
app.use(morgan('dev'))

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

//Serves all the request which includes /images in the url from Images folder
app.use(express.static(__dirname + '/public'));

//Routes
app.get('/', (req, res) => {
  res.render("./src/public/index");
})

app.use(require('./routes/gems'))
module.exports = app