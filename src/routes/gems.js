const express = require('express')
const router = express.Router()
const GemsController = require('../controllers/gemsControllers/gemsController')

let multipart = require('connect-multiparty');

router.use(multipart({
    uploadDir: 'src/tmp'
}));

router.get("/api/gems", GemsController.getAllGems)
router.get("/api/gem/:_id", GemsController.getGem)
router.post("/api/igems", GemsController.insertGem)
router.patch("/api/ugems", GemsController.updateGem)
router.delete("/api/dgems/:_id", GemsController.deleteGem)
router.post("/api/rgems", GemsController.insertReview)
router.post("/api/uimages", GemsController.uploadImagetoGem)

module.exports = router;
