const express = require('express')
const path = require('path')
const app = require('./app')

//Server initialize
app.listen(app.get('port'), ()=>{
    console.log('Server on port', app.get('port'))
})